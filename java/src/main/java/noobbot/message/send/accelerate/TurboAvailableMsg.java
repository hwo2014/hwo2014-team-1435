package noobbot.message.send.accelerate;

import noobbot.message.send.SendMsg;

public class TurboAvailableMsg extends SendMsg
{
	//TODO: send object
	Object value;
	/*
	"turboDurationMilliseconds": 500.0,
	"turboDurationTicks": 30,
	"turboFactor": 3.0
	*/

	public TurboAvailableMsg(double value)
	{
		this.value = value;
	}

	@Override
	protected Object msgData()
	{
		return value;
	}

	@Override
	protected String msgType()
	{
		return "turboAvailable";
	}
}