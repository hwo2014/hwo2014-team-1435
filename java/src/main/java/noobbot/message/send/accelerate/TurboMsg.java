package noobbot.message.send.accelerate;

import noobbot.message.send.SendMsg;

public class TurboMsg extends SendMsg
{
	Object turboString = "Pow pow pow pow pow, or your of personalized turbo message";

	public TurboMsg()
	{
	}

	@Override
	protected Object msgData()
	{
		return turboString;
	}

	@Override
	protected String msgType()
	{
		return "turbo";
	}
}