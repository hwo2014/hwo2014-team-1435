package noobbot.message.send.accelerate;

import noobbot.message.send.SendMsg;

public class ThrottleMsg extends SendMsg
{
	private double value;

	public ThrottleMsg(double value)
	{
		this.value = value;
	}

	@Override
	protected Object msgData()
	{
		return value;
	}

	@Override
	protected String msgType()
	{
		return "throttle";
	}
}