package noobbot.message.send;

public class SwitchLaneMsg extends SendMsg
{
	private LaneSide laneSide;
	
	public enum LaneSide
	{
		Right("Right"), Left("Left");
		
		private final String text;
		
		private LaneSide(final String text)
		{
			this.text = text;
		}

		@Override
		public String toString()
		{
			return text;
		}
	}

	public SwitchLaneMsg(LaneSide laneSide)
	{
		this.laneSide = laneSide;
	}

	@Override
	protected Object msgData()
	{
		return laneSide;
	}

	@Override
	protected String msgType()
	{
		return "switchLane";
	}
}