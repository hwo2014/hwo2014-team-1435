package noobbot.message.send;

public class Ping extends SendMsg
{
	@Override
	protected String msgType()
	{
		return "ping";
	}
}