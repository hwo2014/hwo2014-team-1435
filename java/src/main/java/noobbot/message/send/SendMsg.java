package noobbot.message.send;

import com.google.gson.Gson;

public abstract class SendMsg extends Msg
{
	protected Object msgData()
	{
		return this;
	}
	
	public String toJson()
	{
		return new Gson().toJson(new MsgWrapper(this));
	}
}