package noobbot.message.send.join;

import noobbot.message.send.SendMsg;
import noobbot.model.send.BotId;


public class JoinRaceMsg extends SendMsg
{
	final BotId botId;
	
	final String trackName;// = "hockenheim";
	final String password;// = "schumi4ever";
	final int carCount;// = 3;
	
	public JoinRaceMsg(final BotId botId, final String trackName, final String password, final int carCount)
	{
		this.botId = botId;	
		
		this.trackName = trackName;
		this.password = password; 
		this.carCount = carCount;
	}
	
	@Override
	protected String msgType()
	{
		return "joinRace";
	}
	
	/*	
 		{"msgType": "joinRace", "data": {
		  "botId": {
		    "name": "keke",
		    "key": "IVMNERKWEW"
		  },
		  "trackName": "hockenheim",
		  "password": "schumi4ever",
		  "carCount": 3
		}}
	*/
	
	/*
	   {"msgType": "joinRace", "data": {
		  "botId": {
		    "name": "keke",
		    "key": "IVMNERKWEW"
		  },
		  "carCount": 3
		}}
	 */
}