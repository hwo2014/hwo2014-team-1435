package noobbot.message.send.join;

import noobbot.message.send.SendMsg;

public class JoinMsg extends SendMsg
{
	public final String name;
	public final String key;

	public JoinMsg(final String name, final String key)
	{
		this.name = name;
		this.key = key;
	}

	@Override
	protected String msgType()
	{
		return "join";
	}
}