package noobbot.message.receive;

import noobbot.model.GameEnd;

public class GameEndMsg extends ReceiveMsg<GameEnd>
{
	public GameEndMsg(Object data)
	{
		super(data, GameEnd.class);
	}

	@Override
	protected String msgType()
	{
		return "gameEnd";
	}
}