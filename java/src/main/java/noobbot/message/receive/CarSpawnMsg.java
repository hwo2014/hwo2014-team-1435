package noobbot.message.receive;

import noobbot.model.CarId;

public class CarSpawnMsg extends ReceiveMsg<CarId>
{
	public CarSpawnMsg(Object data)
	{
		super(data, CarId.class);
	}

	@Override
	protected String msgType()
	{
		return "spawn";
	}
}