package noobbot.message.receive;

import noobbot.model.Disqualified;

public class DisqualifiedMsg extends ReceiveMsg<Disqualified>
{
	public DisqualifiedMsg(Object data)
	{
		super(data, Disqualified.class);
	}

	@Override
	protected String msgType()
	{
		return "dnf";
	}
}