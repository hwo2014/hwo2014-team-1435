package noobbot.message.receive;

import noobbot.model.CarId;

public class CarCrashMsg extends ReceiveMsg<CarId>
{
	public CarCrashMsg(Object data)
	{
		super(data, CarId.class);
	}

	@Override
	protected String msgType()
	{
		return "crash";
	}
}