package noobbot.message.receive;

import noobbot.message.send.Msg;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public abstract class ReceiveMsg<T> extends Msg
{
	private T object;
	
	protected ReceiveMsg(Object data, Class<T> clazz)
	{
		object = fromJson(data, clazz);
	}
	
	protected T fromJson(Object data, Class<T> clazz)
	{	
		if (data != null)
		{
			T t = null;
			try
			{
				t = (T) new Gson().fromJson(data.toString(), clazz);
			}
			catch(JsonSyntaxException ex)
			{
				System.out.println(ex.toString() + "/n" + data.toString());
			}
			return t;
		}
		else
		{
			System.out.println("Null Data for class: " + clazz.toString());
			return null; //T extends Model; 
						 //return VoidModel; 
		}
	}
	
	public T getObject()
	{
		return object;
	}
}