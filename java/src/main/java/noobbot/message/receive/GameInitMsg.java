package noobbot.message.receive;

import noobbot.model.GameInit;

public class GameInitMsg extends ReceiveMsg<GameInit>
{
	public GameInitMsg(Object data)
	{
		super(data, GameInit.class);
	}

	@Override
	protected String msgType()
	{
		return "gameInit";
	}
}