package noobbot.message.receive;

import noobbot.model.GameStart;

public class GameStartMsg extends ReceiveMsg<GameStart>
{
	public GameStartMsg(Object data)
	{
		super(data, GameStart.class);
	}

	@Override
	protected String msgType()
	{
		return "gameStart";
	}
}