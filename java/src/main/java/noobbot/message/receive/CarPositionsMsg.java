package noobbot.message.receive;

import noobbot.model.CarPosition;

public class CarPositionsMsg extends ReceiveMsg<CarPosition[]>
{
	public CarPositionsMsg(Object data)
	{
		super(data, CarPosition[].class);
	}

	@Override
	protected String msgType()
	{
		return "carPositions";
	}
}