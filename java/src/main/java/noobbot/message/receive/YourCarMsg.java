package noobbot.message.receive;

import noobbot.model.CarId;

public class YourCarMsg extends ReceiveMsg<CarId>
{
	public YourCarMsg(Object data)
	{
		super(data, CarId.class);
	}

	@Override
	protected String msgType()
	{
		return "yourCar";
	}
}