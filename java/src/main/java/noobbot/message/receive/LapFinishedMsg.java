package noobbot.message.receive;

import noobbot.model.LapFinished;

public class LapFinishedMsg extends ReceiveMsg<LapFinished>
{
	public LapFinishedMsg(Object data)
	{
		super(data, LapFinished.class);
	}

	@Override
	protected String msgType()
	{
		return "lapFinished";
	}	
}