package noobbot.model;

public class PiecePosition
{
    public int pieceIndex;    		//pieceIndex - zero based index of the piece the car is on
    public float inPieceDistance;	//inPieceDistance - the distance the car's Guide Flag (see above) has traveled from the start of the piece along the current lane
    public LanePosition lane;		//lane - a pair of lane indices. 
    
    @Override
	public String toString()
	{
		return "pieceIndex " + pieceIndex + " inPieceDistance " + inPieceDistance + " lane " + lane;
	}
}