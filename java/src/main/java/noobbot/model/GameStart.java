package noobbot.model;

public class GameStart
{
	//{"msgType": "gameStart", "data": null, "gameTick": 0}
	//The race has started! 
	//The gameTick attribute indicates the current Server Tick, which is an increasing integer. 
	//For gameStart the gameTick is always zero.
	
	public String gameId;
	public int gameTick;
}
