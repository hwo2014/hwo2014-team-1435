package noobbot.model;

public class Lane
{
	public int index;
	public int distanceFromCenter;
    
    @Override
    public String toString()
    {
    	return "[ index: " + index + ", distanceFromCenter: " + distanceFromCenter + "]";
    }
}