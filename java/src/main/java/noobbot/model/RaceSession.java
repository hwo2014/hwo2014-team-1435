package noobbot.model;

public class RaceSession
{
	public int laps;
	public int maxLapTimeMs;
	public boolean quickRace;
}