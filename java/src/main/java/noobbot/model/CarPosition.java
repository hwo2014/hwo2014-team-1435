package noobbot.model;

public class CarPosition
{
	public CarId id;
	public int lap; 		//lap - the number of laps the car has completed. The number 0 indicates that the car is on its first lap. The number -1 indicates that it has not yet crossed the start line to begin it's first lap.
	
	public float angle;	//angle - depicts the car's slip angle. Normally this is zero, but when you go to a bend fast enough, the car's tail will start to drift. Naturally, there are limits to how much you can drift without crashing out of the track.
	public PiecePosition piecePosition;
	
	@Override
	public String toString()
	{
		return id.name + "[ angle:" + angle + ", " + piecePosition + "]";
	}
}