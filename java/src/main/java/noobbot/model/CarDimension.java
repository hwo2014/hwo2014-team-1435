package noobbot.model;

public class CarDimension
{
	float length;
	float width;
	float guideFlagPosition;
	
	@Override
	public String toString()
	{
		return "CarDimension: [ length: " + length +", width: " + width + ", guideFlagPosition: " + guideFlagPosition +"]"; 
	}
}