package noobbot.model;

public class CarResult
{
	CarId car;
	LapResult result;
	
	@Override
	public String toString()
	{
		return "CarResult: [" + car + ", " + result + "]";
	}
}
