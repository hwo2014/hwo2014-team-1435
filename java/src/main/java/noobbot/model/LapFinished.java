package noobbot.model;

public class LapFinished
{
	CarId car;
	
	LapResult lapTime;
	LapResult raceTime;
	CarRanking ranking;
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "[" + " car :" + car + " laptime: " + lapTime + " racetime: " + raceTime + " ,ranking: " + ranking;
	}
	
/*	"car": {
    "name": "Schumacher",
    "color": "red"
  },
  "lapTime": {
    "lap": 1,
    "ticks": 666,
    "millis": 6660
  },
  "raceTime": {
    "laps": 1,
    "ticks": 666,
    "millis": 6660
  },
  "ranking": {
    "overall": 1,
    "fastestLap": 1
  }*/
}
