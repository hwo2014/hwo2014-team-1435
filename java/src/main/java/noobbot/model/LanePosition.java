package noobbot.model;

public class LanePosition
{
	public int startLaneIndex;
	public int endLaneIndex;
    //Usually startLaneIndex and endLaneIndex are equal, but they do differ when the car is currently switching lane
    
    @Override
	public String toString()
	{
		return "startLaneIndex: " + startLaneIndex + " endLaneIndex: " + endLaneIndex;
	}
}