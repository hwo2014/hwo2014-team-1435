package noobbot.model;

import com.google.gson.annotations.SerializedName;

public class Piece
{
	public float length;
	public int radius;
	public float angle;
	@SerializedName("switch")
	public boolean switchSide;
	
	@Override
	public String toString()
	{
		return "Piece: [ length: " + length + ", radius: " + radius 
					 + ", angle: " + angle + ", switch: "+ switchSide + "]";
	}
}