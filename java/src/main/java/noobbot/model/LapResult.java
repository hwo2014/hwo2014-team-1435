package noobbot.model;

public class LapResult
{
	int laps;
	int ticks;
	int millis;
	
	@Override
	public String toString()
	{
		return "LapResult: [ laps: " + laps + ", ticks: " + ticks + ", millis: " + millis +"]";
	}
}
