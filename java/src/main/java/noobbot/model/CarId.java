package noobbot.model;

public class CarId
{
	public String name;
	public String color;
	
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null )
		{
			return false;
		}
		
		if( this == obj )
		{
			return true;
		}
		
		if( !(obj instanceof CarId) )
		{
			return false;
		}
		
		CarId carId = (CarId) obj;
		
		return this.name.equals(carId.name) && this.color.equals(carId.color);
	}
	
	@Override
	public String toString()
	{
		return "CarId: [name:" + name + ", color: " + color + "]";
	}
}