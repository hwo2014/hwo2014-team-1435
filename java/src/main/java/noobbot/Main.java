package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import javax.print.attribute.standard.Sides;

import noobbot.message.receive.CarPositionsMsg;
import noobbot.message.receive.DisqualifiedMsg;
import noobbot.message.receive.GameEndMsg;
import noobbot.message.receive.GameInitMsg;
import noobbot.message.receive.GameStartMsg;
import noobbot.message.receive.LapFinishedMsg;
import noobbot.message.receive.YourCarMsg;
import noobbot.message.send.MsgWrapper;
import noobbot.message.send.Ping;
import noobbot.message.send.SendMsg;
import noobbot.message.send.SwitchLaneMsg;
import noobbot.message.send.SwitchLaneMsg.LaneSide;
import noobbot.message.send.accelerate.ThrottleMsg;
import noobbot.message.send.accelerate.TurboMsg;
import noobbot.message.send.join.CreateRaceMsg;
import noobbot.message.send.join.JoinMsg;
import noobbot.message.send.join.JoinRaceMsg;
import noobbot.model.CarDimension;
import noobbot.model.CarId;
import noobbot.model.CarPosition;
import noobbot.model.Lane;
import noobbot.model.Piece;
import noobbot.model.Race;
import noobbot.model.send.BotId;
import noobbot.simulation.CarUtil;
import noobbot.simulation.PhysicUtil;
import noobbot.simulation.PieceUtils;

import com.google.gson.Gson;

//https://helloworldopen.com/race/535f0ddde4b0e941ca7282ff
//https://helloworldopen.com/race/535f169fe4b0c6663c3c8b99
//https://helloworldopen.com/race/535f1dd3e4b0e941ca7287fa
//https://helloworldopen.com/race/535f1e69e4b0e941ca72883c

//https://helloworldopen.com/race/535f1f18e4b0c6663c3c8e4c

public class Main
{
	public static void main(String... args) throws IOException
	{
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		Socket socket = null;
		PrintWriter writer = null;
		BufferedReader reader = null;
		try
		{
			socket = new Socket(host, port);
			writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
			new Main(reader, writer, new JoinMsg(botName, botKey));
			//new Main(reader, writer, new NameKeyJoin());
		}
		catch(Exception e)
		{
			System.out.println("Exception: " + e.toString());
		}
		finally
		{
			if( writer != null )
			{
				writer.close();
			}

			if( reader != null )
			{
				reader.close();
			}

			if( socket != null )
			{
				socket.close();
			}
		}
		
		System.out.println("MAIN APP END.");
	}

	final Gson gson = new Gson();
	private PrintWriter writer;

	//
	// Current model of Game
	//
	CarId myCarId;
	CarDimension myCarDimension;
	Race race;
	
	int minLaneIndex = 0;
	double minLaneDistance = 0;
	LaneSide actualSide = LaneSide.Left;
	
	double lastInPieceDistance = 0;
	int lastPieceId = 0;
	
	double computeMaxVelocityBeforeSlipping = 1.0f;
	double throttle = 1.0f;
	//
	//
	//
	boolean wasTurbo = false;
	private boolean wasCreateRace = false;
	private boolean wasJoinRace = false;
	int tick = 0;
	private int lastSwitchPieceId = 0;
	
	
	public Main(final BufferedReader reader, final PrintWriter writer, final JoinMsg join) throws IOException
	{
		this.writer = writer;
		String line = null;
		
		final BotId botId = new BotId(join.name, join.key);
		final String trackName = "france";
		final String password = "schumi4ever";
		final int carCount = 2;
		
		final BotId botId2 = new BotId("Flakac", "join.key");

		send(join);
		
//		if(!wasCreateRace)
//		{
//			wasCreateRace = true;
//			send(new CreateRaceMsg(botId, trackName, password, carCount));
//		}		
		
		while( (line = reader.readLine()) != null )
		{
			tick++;
			
			line = line.replaceAll("\\s+", "");//fix prevent crash, when team name is without quota
			final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
			
			System.out.println("Tick: " + tick);
			if( msgFromServer.msgType.equals("carPositions") )
			{
				CarPositionsMsg posMsg = new CarPositionsMsg(msgFromServer.data);
				
				CarPosition[] positions = posMsg.getObject();
				CarPosition myPos = CarUtil.getMyCarPosition(myCarId, positions);
				System.out.println("Car positions: " + myPos.toString());
				
				int curPieceId = myPos.piecePosition.pieceIndex;
				Piece curPieceInfo = race.track.pieces[(curPieceId+1)%race.track.pieces.length];
				
				if( curPieceInfo.switchSide == true && curPieceId != lastSwitchPieceId )
				{
					lastSwitchPieceId = curPieceId;
					Lane curLane = PieceUtils.getLaneForIndex(race.track.lanes, myPos.piecePosition.lane.startLaneIndex);
					//if( curLane.index != minLaneIndex )
					{
						LaneSide side;
						//if(curLane.index < minLaneIndex)
						//if(curLane.distanceFromCenter < minLaneIndex)
						if(actualSide == LaneSide.Left)
						{
							side = LaneSide.Right;
							actualSide = side;
						}
						else
						{
							side = LaneSide.Left;
							actualSide = side;
						}
						send(new SwitchLaneMsg(side));
					}
				}
				
				if( lastPieceId != curPieceId )
				{
					Piece lastPieceInfo = race.track.pieces[lastPieceId];
					Lane actualLane = PieceUtils.getLaneForIndex(race.track.lanes, myPos.piecePosition.lane.startLaneIndex);
					double lastPieceLenght = PieceUtils.getPieceLength(lastPieceInfo, actualLane.distanceFromCenter);
					double deltaBetween2InPieceDistance = lastPieceLenght - lastInPieceDistance;
					lastInPieceDistance = -1 * deltaBetween2InPieceDistance;	//reset distance, when go to new piece
					lastPieceId = curPieceId;	//set correct piece id
					
					System.out.println("lastPieceLenght: " + lastPieceLenght);
				}
				
				double currentInPieceDistance = myPos.piecePosition.inPieceDistance;
				double deltaInPieceDistance = currentInPieceDistance - lastInPieceDistance;
				lastInPieceDistance = currentInPieceDistance;
				
				//System.out.println("PieceIndex: " + curPieceId + " " + pInfo);;
				
				double carVelocity = deltaInPieceDistance;//lastInPieceDistance;
				//System.out.println("carVelocity: " + carVelocity);
				//System.out.println("lastInPieceDistance: " + lastInPieceDistance);
				
				double currentCarAngleDeg = myPos.angle;
				/*double cornerRadius = PieceUtils.getPieceRadius(curPieceInfo, PieceUtils.getLaneForIndex(race.track.lanes, myPos.piecePosition.lane.startLaneIndex).distanceFromCenter);
				if( cornerRadius != 0 )
				{
					computeMaxVelocityBeforeSlipping = PhysicUtil.computeMaxVelocityBeforeSlipping(carVelocity, cornerRadius, currentCarAngleDeg);
				}
				else
				{
					computeMaxVelocityBeforeSlipping = 1.0f;
				}*/
				
				//double throttle = computeMaxVelocityBeforeSlipping;
				if( myPos != null )
				{
					int countPieces = race.track.pieces.length;
					//Piece curPieceInfo_1 = race.track.pieces[(curPieceId+1)%countPieces];
					//Piece curPieceInfo_2 = race.track.pieces[(curPieceId+2)%countPieces];
					//Piece curPieceInfo_3 = race.track.pieces[(curPieceId+3)%countPieces];
					//Piece curPieceInfo_4 = race.track.pieces[(curPieceId+4)%countPieces];
					
					//if (computeMaxVelocityBeforeSlipping == 1.0)
					{
						double avgMaxVelocity = 0;
						int countCorner = 0;
						
						//Double d = Double.valueOf(deltaInPieceDistance);
						//int carVelocityINT = d.intValue();
						int predictPiece = Long.valueOf(Math.round(carVelocity*carVelocity/7)).intValue();
						if(predictPiece == 0)
						{
							predictPiece = 0;
						}
						System.out.println("Count Predict Piece: " + predictPiece /*+ " carVelocityINT:" + carVelocityINT*/);
						
						//double maxVelBefSplip[] = new double[predictPiece];
						for(int i = 0, j = 0; j < predictPiece; j++, i++)
						{
							//maxVelBefSplip[i] = 0.0;
							Piece piece = race.track.pieces[(curPieceId+i)%countPieces]; 
							double cornerRadius_ = PieceUtils.getPieceRadius(piece, PieceUtils.getLaneForIndex(race.track.lanes, myPos.piecePosition.lane.startLaneIndex).distanceFromCenter);
							if( cornerRadius_ != 0 )
							{
								countCorner++;
								double maxVelBefSplip = PhysicUtil.computeMaxVelocityBeforeSlipping(carVelocity, cornerRadius_, currentCarAngleDeg);
								avgMaxVelocity += maxVelBefSplip;
								System.out.println("Piece: " + (curPieceId+i)%countPieces + ".  maxVelBefSplip:"+ maxVelBefSplip );
							}
							else
							{
								//j--;
							}
						}
					
						if( avgMaxVelocity != 0 )
						{
							avgMaxVelocity /= countCorner;
						}
						
						if( (avgMaxVelocity < carVelocity || avgMaxVelocity == 0) && predictPiece < 9 )
						{
							throttle += 0.3;
							if(throttle > 1.0)
							{
								throttle = 1.0;
							}
						}
						else
						{
							double multiplerVelocity = Math.sqrt(avgMaxVelocity / carVelocity / 2);
							if(multiplerVelocity < 1.0)
							{
								multiplerVelocity = 1.0;
							}
							System.out.println("multiplerVelocity: " + multiplerVelocity);
							
							throttle -= 0.18 * countCorner;// * multiplerVelocity;
							double minlimit = /*1.3599*/ 1.0 - 0.18 * countCorner;// * multiplerVelocity;
							if(minlimit < 0)
							{
								minlimit = 0.0;
							}
							if(throttle < minlimit)
							{
								throttle = minlimit;
							}
						}
				
						System.out.println("carVelocity: " + carVelocity);
						System.out.println("avgMaxVelocity: " + avgMaxVelocity);
					}
							
//					for(int i = 0; i < race.track.pieces.length; i++)
//					{
//						Piece piece = race.track.pieces[i]; 
//						double cornerRadius_ = piece.radius;
//						if( cornerRadius_ != 0 )
//						{
//							System.out.println("Piece: " + i + ".");
//							double computeMaxVelocityBeforeSlipping = PhysicUtil.computeMaxVelocityBeforeSlipping(carVelocity, cornerRadius_, currentCarAngleDeg);
//						}
//					}
					
//					if(Math.abs(curPieceInfo_1.angle) > 30 && Math.abs(curPieceInfo_2.angle) > 30)
//					{
//						throttle = 0.1;
//					}
					//throttle = computeMaxVelocityBeforeSlipping;
				}
				
				if(curPieceId == 0)
				{
					wasTurbo = false;
				}
				
				if(curPieceId == 34 && !wasTurbo)
				{
					send(new TurboMsg());
					wasTurbo = true;
				}
				else
				{
					send(new ThrottleMsg(throttle));
				}	
				
				System.out.println("====================================");
				System.out.println("Thortle: " + throttle);
				System.out.println("====================================");
			}
			else{
				if( msgFromServer.msgType.equals("yourCar") )
				{
					System.out.println("Your car");
					
					YourCarMsg myCarMsg = new YourCarMsg(msgFromServer.data);
					myCarId = myCarMsg.getObject();
					System.out.println("Your car: " + myCarMsg.getObject() );
				}
				else if( msgFromServer.msgType.equals("gameStart") )
				{
					System.out.println("Race start");
					
					GameStartMsg gameStartMsg = new GameStartMsg(msgFromServer.data);
					System.out.println("GameStart Object: " + gameStartMsg.getObject());
				} 
				else if( msgFromServer.msgType.equals("gameInit") )
				{
					System.out.println("Race init");
					
					GameInitMsg gameInitMsg = new GameInitMsg(msgFromServer.data);
					race = gameInitMsg.getObject().race;
					myCarDimension = CarUtil.getMyCarDimension(myCarId, race.cars);
					
					
					for( Lane lane: race.track.lanes )
					{
						if( lane.distanceFromCenter < minLaneDistance )
						{
							minLaneDistance = lane.distanceFromCenter;
							minLaneIndex = lane.index;
						}
					}
					
					System.out.println("GameInit Object: " + gameInitMsg.getObject());
					System.out.println("myCarDimension: " + myCarDimension);
				} 
				else if( msgFromServer.msgType.equals("gameEnd") )
				{
					System.out.println("Race end");
					
					GameEndMsg gameEndMsg = new GameEndMsg(msgFromServer.data);
					System.out.println("GameEnd Object: " + gameEndMsg.getObject());
				}
				else if( msgFromServer.msgType.equals("lapFinished") )
				{
					System.out.println("lapFinished");
					
					LapFinishedMsg lapMsg = new LapFinishedMsg(msgFromServer.data);
					System.out.println("LapFinishMsg " + lapMsg.getObject());
				}
				else if( msgFromServer.msgType.equals("dnf") )
				{
					System.out.println("dnf");
					DisqualifiedMsg dnfMsg = new DisqualifiedMsg(msgFromServer.data);
					
					System.out.println("DisqualifiedMsg " + dnfMsg.getObject());
				}
				else if( msgFromServer.msgType.equals("join") )
				{
					System.out.println("Joined");
				}
				else if( msgFromServer.msgType.equals("createRace") )
				{
					Object obj = msgFromServer.data;
					System.out.println("Create Race: " + obj);
					if(!wasJoinRace)
					{
						System.out.println("Send JoinRace");
						wasJoinRace = true;
						send(new JoinRaceMsg(botId2, trackName, password, carCount));
					}
				}
				else if( msgFromServer.msgType.equals("joinRace"))
				{
					System.out.println("Join Race");
				}
				
				send(new Ping());
			}
		}
	}

	private void send(final SendMsg msg)
	{
		String json = msg.toJson();
		writer.println(json);
		writer.flush();
	}
}