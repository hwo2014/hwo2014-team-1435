package noobbot.simulation;

import noobbot.model.CarDimension;
import noobbot.model.CarId;
import noobbot.model.CarInfo;
import noobbot.model.CarPosition;

public class CarUtil
{
	static public CarPosition getMyCarPosition(CarId myCarId, CarPosition[] otherCarPositions)
	{
		int carCount = otherCarPositions.length;
		for(int i = 0; i < carCount; i++)
		{
			CarPosition carPositon = otherCarPositions[i];
			CarId currentCarId = carPositon.id;
			if(currentCarId.equals(myCarId))
			{
				return carPositon;
			}
		}
		
		return null;
	}
	
	static public CarDimension getMyCarDimension(CarId myCarId, CarInfo[] otherCarInfo)
	{
		int carCount = otherCarInfo.length;
		for(int i = 0; i < carCount; i++)
		{
			CarInfo carInfo = otherCarInfo[i];
			CarId currentCarId = carInfo.id;
			if(currentCarId.equals(myCarId))
			{
				return carInfo.dimensions;
			}
		}
		
		return null;
	}
}
