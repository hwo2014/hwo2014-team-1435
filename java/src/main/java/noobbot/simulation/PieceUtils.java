package noobbot.simulation;

import noobbot.model.Lane;
import noobbot.model.Piece;

public class PieceUtils
{
	static public double getPieceLength(Piece piece, int laneDeltaRadius)
	{
		if(piece.angle == 0)
		{
			return piece.length;
		}
		else
		{
			//http://www.aristoteles.cz/matematika/planimetrie/kruznice.php
			double angle = Math.abs(piece.angle);
			if( piece.angle < 0 )
			{
				laneDeltaRadius *= -1.0;	//WTF?!?
			}
			double radius = piece.radius - laneDeltaRadius;
			double leghtCircleCut = angle * 2 * Math.PI * radius / 360.0;
			//System.out.println("leghtCircleCut " + leghtCircleCut + " [ angle: " + piece.angle + " radius: " + piece.radius + ", laneDeltaRadius:" + laneDeltaRadius +"]");
			return leghtCircleCut;
		}
	}

	public static Lane getLaneForIndex(Lane[] lanes, int startLaneIndex)
	{
		int laneCount = lanes.length;
		for(int i = 0; i < laneCount; i++)
		{
			Lane lane = lanes[i];
			
			if( lane.index == startLaneIndex )
			{
				return lane;
			}
		}
		
		return null;
	}
	
	static public double getPieceRadius(Piece piece, int laneDeltaRadius)
	{
			if (piece.radius == 0)
			{
				return 0;
			}
			else
			{
				if( piece.angle < 0 )
				{
					laneDeltaRadius *= -1.0;	//WTF?!?
				}
				double radius = piece.radius - laneDeltaRadius;
				
				return radius;
			}
	}
}
