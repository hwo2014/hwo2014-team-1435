package noobbot.simulation;

public class PhysicUtil
{
	//http://www.reddit.com/r/HWO/comments/245yms/how_to_calculate_speed_at_which_you_start_slipping/
/*
Throttle up and go into a corner. On the tick when your very first slide is detected
, note down your speed (V), corner radius (R) and current angle (A) in degrees.

Your current angular velocity in degrees (B) will be 
B = RadToDeg(V/R) 
This value is directly related to the first slip angle.

Therefore your maximum angular velocity before slipping (in degrees) is 
Bslip = B - A;

Knowing this you can calculate your threshold speed 
Vthres = DegToRad(Bslip ) * R;

Therefore the max centrifugal force (Fslip ) the car can take before sliding is:
Fslip = Vthres * Vthres / R;
Store Fslip.

For every corner of radius R (make sure you use the correct lane offset), your maximum velocity before slipping is:
VmaxNoSlip = sqrt(Fslip * R)
*/
	
//	Your current angular velocity in degrees (B) will be 
//	B - angularVelocityDeg 
//	V - speedVelocity
//	R - cornerRadius
//
//	B = RadToDeg(V/R) 
//	This value is directly related to the first slip angle.
	public static double computeAngularVelocityDeg(double carVelocity, double cornerRadius) 
	{
		double angularVelocityDeg = Math.toDegrees(carVelocity / cornerRadius); 
		return angularVelocityDeg;
	}
	
//	Therefore your maximum angular velocity before slipping (in degrees) is 
//	Bslip - maxAngularVelocityDeg
//	A - currentAngleDeg - current angle in degrees
//
//	Bslip = B - A;
	public static double computeMaxAngularVelocityDeg(double angularVelocityDeg, double currentCarAngleDeg)
	{
		double maxAngularVelocityDeg = angularVelocityDeg - currentCarAngleDeg;
		return	maxAngularVelocityDeg; 
	}

//	Knowing this you can calculate your threshold speed 
//	Vthres - threshSpeed
//
//	Vthres = DegToRad(Bslip) * R;
	public static double computeThreshSpeed(double maxAngularVelocityDeg, double cornerRadius)
	{
		double threshSpeed = Math.toRadians(maxAngularVelocityDeg) * cornerRadius;
		return threshSpeed;
	}
	
//	Therefore the max centrifugal force (Fslip ) the car can take before sliding is:
//	Fslip - maxCentrifugalForce
//	
//	Fslip = Vthres * Vthres / R;
//	Store Fslip.
	public static double computeMaxCentrifugalForce(double threshSpeed, double cornerRadius)
	{
		double maxCentrifugalForce = threshSpeed * threshSpeed / cornerRadius;
		return maxCentrifugalForce;
	}
	
//	For every corner of radius R (make sure you use the correct lane offset)
//	, your maximum velocity before slipping is:
//	VmaxNoSlip - maxVelocityBeforeSlip
//		
//	VmaxNoSlip = sqrt(Fslip * R)
	public static double computeMaxVelocityBeforeSlipping(double maxCentrifugalForce, double cornerRadius)
	{
		double maxVelocityBeforeSlip = Math.sqrt(maxCentrifugalForce * cornerRadius);
		return maxVelocityBeforeSlip;
	}
	
	public static double computeMaxVelocityBeforeSlipping(double carVelocity, double cornerRadius, double currentCarAngleDeg)
	{
		double angularVelocityDeg = PhysicUtil.computeAngularVelocityDeg(carVelocity, cornerRadius);
		double maxAngularVelocityDeg = PhysicUtil.computeMaxAngularVelocityDeg(angularVelocityDeg, currentCarAngleDeg);
		double threshSpeed = PhysicUtil.computeThreshSpeed(maxAngularVelocityDeg, cornerRadius);
		double maxCentrifugalForce = PhysicUtil.computeMaxCentrifugalForce(threshSpeed, cornerRadius);
		double computeMaxVelocityBeforeSlipping = PhysicUtil.computeMaxVelocityBeforeSlipping(maxCentrifugalForce, cornerRadius);
		
		//System.out.println("[carVelocity: " + carVelocity + ", cornerRadius: " + cornerRadius +", angularVelocityDeg: " + angularVelocityDeg + "]");
//		System.out.println("--------------------------------------------------------");					
//		System.out.println("maxAngularVelocityDeg: " + maxAngularVelocityDeg + "\n"
//				+ "threshSpeed: " + threshSpeed + "\n"
//				+ "maxCentrifugalForce: " + maxCentrifugalForce + "\n"
//				+ "computeMaxVelocityBeforeSlipping: " + computeMaxVelocityBeforeSlipping);
//		System.out.println("--------------------------------------------------------");
		
		return computeMaxVelocityBeforeSlipping;
	}

///-----------------------------------------------------------------------------------------------------------------///
//http://www.reddit.com/r/HWO/comments/245srp/speed_dynamics/	
//For those who have yet to figure out the speed dynamics:
//In order to calculate how the car behaves given a throttle value, you need two constants:
//k: Drag constant
//h: Is the current throttle (Set it to 1.0 or at least keep it constant on the the first three ticks)
//All you need to calculate these constants is the speed v(t) on the first 3 ticks after the car started moving (Speed v(0) has to be 0.0).
//    k = ( v(1) - ( v(2) - v(1) ) ) / v(1)^2 * h;
	public static double compute_K_DragConstant(double v1, double v2, double h)
	{
		double k = ( v1 - ( v2 - v1 ) ) / v1 * v1 * h;
		return k;
	}
	
//	Now that you have k you can calculate the mass:
//	    m = 1.0 / ( ln( ( v(3) - ( h / k ) ) / ( v(2) - ( h / k ) ) ) / ( -k ) )
//	m: Mass of the car
	public static double compute_M_MassOfCar(double v2, double v3, double h, double k)
	{
		double m = 1.0 / ( Math.log( ( v3 - ( h / k ) ) / ( v2 - ( h / k ) ) ) / ( -k ) );
		return m;
	}
	
//	With both constants calculated you can start doing interesting things.
//	    Calculate the terminal velocity v for a given throttle:
//	    v = h/k
	public static double compute_V_velocityHK(double h, double k)
	{
		double v = h/k;
		return v;
	}
	
//	    Calculate your speed in a given amount of ticks t:
//	    v(t) = (v(0) - (h/k) ) * e^ ( ( - k * t ) / m ) + ( h/k )
/*	public static double compute_V_velocityT(int t, double m, double v0, double h, double k)
	{
		double vt = (v0 - (h/k) ) * Math.pow(Math.E, ( ( - k * t ) / m )) + ( h/k );
		return vt;
	}*/
	
//	v(0): Here v(0) is the velocity you currently have
//	    How many ticks do you need to get to a given speed v:
//	    t = ( ln ( (v - ( h/k ) )/(v(0) - ( h/k ) ) ) * m ) / ( -k )
//	You should round this value to the next integer
//	    Distance traveled in t ticks:
//	    d(t) = ( m/k ) * ( v(0) - ( h/k ) ) * ( 1.0 - e^ ( ( -k*t ) / m ) ) + ( h/k ) * t + d(0)
//
//	d(0): Your current position, 0.0 if you want a relative distance
//	    Forgot your throttle or want to know the throttle an opponent has set (you need two speed mesurements [t is the amount of ticks between them, works best if t is 1]):
//	    h = ( k * ( v(t) * e^ ( ( k * t ) / m ) - v(0) ) / ( e^ ( (k * t) /m ) - 1.0 ) )
//
//	All equations also work with the turbo, you just have to multiply your throttle by the given amount. Also don't try to figure out how long it will take to reach terminal velocity for a given throttle, you never reach it completly.
//	Now if you want to know how fast you can drive through a curve segment (max constant speed, you could go faster in the beginning), you need another constant
//	c: Maximum centrifugal force the car can put onto the track
//
//	You can't calculate c however (or i have't figured out how), you have to accelerate in a curve segment until you crash. You want to know the speed v you had right before you crashed.
//	    c = v^2 * r
//	    
//	r: radius of the current lane
//	Now you can calculate the max speed you can drive through given curve without crashing
//	    v = sqrt( c * r )
//	Using only this information and little to no AI you should be able to reach a time of around 7.3 seconds on keimola
}
